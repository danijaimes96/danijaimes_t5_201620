package taller.mundo;

import java.util.Arrays;
import java.util.Comparator;

import taller.estructuras.Heap;

public class Pizzeria
{
	// ----------------------------------
	// Constantes
	// ----------------------------------

	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO
	private Heap<Pedido> pedidosRecibidos;

	/**
	 * Getter de pedidos recibidos
	 */
	// TODO
	public Heap<Pedido> darPedidosRecibidos()
	{
		return pedidosRecibidos;
	}

	/**
	 * Heap de elementos por despachar
	 */
	// TODO
	private Heap<Pedido> elemntosADespachar;

	/**
	 * Getter de elementos por despachar
	 */
	public Heap<Pedido> darElementosADespachar()
	{
		return elemntosADespachar;
	}

	// TODO

	// ----------------------------------
	// Constructor
	// ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		// TODO
		pedidosRecibidos = new Heap<Pedido>();
		elemntosADespachar = new Heap<Pedido>(new Comparator<Pedido>()
		{

			@Override
			public int compare(Pedido p1, Pedido p2)
			{
				return Integer.compare(p2.getCercania(), p1.getCercania());
			}
		});
	}

	// ----------------------------------
	// Métodos
	// ----------------------------------

	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * 
	 * @param nombreAutor
	 *            nombre del autor del pedido
	 * @param precio
	 *            precio del pedido
	 * @param cercania
	 *            cercania del autor del pedido
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO
		Pedido pedido = new Pedido(precio, nombreAutor, cercania);
		pedidosRecibidos.add(pedido);
	}

	// Atender al pedido más importante de la cola

	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * 
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO

		Pedido temp = pedidosRecibidos.poll();
		elemntosADespachar.add(temp);
		return temp;
	}

	/**
	 * Despacha al pedido proximo a ser despachado.
	 * 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO
		return elemntosADespachar.poll();
	}

	/**
	 * Retorna la cola de prioridad de pedidos recibidos como un arreglo.
	 * 
	 * @return arreglo de pedidos recibidos manteniendo el orden de la cola de
	 *         prioridad.
	 */
	public Pedido[] pedidosRecibidosList()
	{
		// TODO
		Pedido[] nueva = new Pedido[pedidosRecibidos.size()];
		for (int i = 0; i < nueva.length && pedidosRecibidos.peek() != null; i++)
		{

			nueva[i] = pedidosRecibidos.poll();
		}
		for (int i = 0; i < nueva.length; i++)
		{
			pedidosRecibidos.add(nueva[i]);
		}
		return nueva;
	}

	/**
	 * Retorna la cola de prioridad de despachos como un arreglo.
	 * 
	 * @return arreglo de despachos manteniendo el orden de la cola de
	 *         prioridad.
	 */
	public Pedido[] colaDespachosList()
	{
		// TODO
		Pedido[] nueva = new Pedido[elemntosADespachar.size()];
		for (int i = 0; i < nueva.length && elemntosADespachar.peek() != null; i++)
		{
			nueva[i] = elemntosADespachar.poll();
		}

		for (int i = 0; i < nueva.length; i++)
		{
			elemntosADespachar.add(nueva[i]);
		}

		return nueva;
	}
}
