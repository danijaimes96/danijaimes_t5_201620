package taller.mundo;

public class Pedido implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	
	public Pedido(double precio, String autorpedido, int cercania)
	{
		this.precio=precio;
		this.autorPedido= autorpedido;
		this.cercania= cercania;
	}
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}

	
	
	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return autorPedido;
	}

	@Override
	public int compareTo(Pedido o) {
		// TODO Auto-generated method stub
		return Double.compare(precio, o.precio);
	}
	
	// TODO 
	
}
