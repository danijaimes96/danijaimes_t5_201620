package taller.estructuras;

import java.util.Arrays;
import java.util.Comparator;
/**
 * Clase Heap que tiene el max heap y el min heap
 * @author Daniela Jaimes
 *
 * @param <T>
 */
public class Heap<T extends Comparable<T>> implements IHeap<T>
{
/**
 * arregle donde se guardan los datos del heap
 */
	private T[] arreglo;
	/**
	 * tamaño del heap/arreglo
	 */
	private int size;
	/**
	 * comparador 
	 */
	private Comparator<T> orden;
/**
 * constructor max heap
 */
	public Heap()
	{
		arreglo = (T[]) new Comparable[1024];
		size = 0;
	}
/**
 * constructor min heap
 * @param c
 */
	public Heap(Comparator<T> c)
	{
		orden = c;
		arreglo = (T[]) new Comparable[1024];
		size = 0;
	}
	/**
	 * Agrega un elemento al heap
	 */
	public void add(T elemento)
	{
		// TODO Auto-generated method stub
		if (size == arreglo.length * 0.87)
		{
			T[] arreglo2 = (T[]) new Comparable[arreglo.length + 1024];
			System.arraycopy(arreglo, 0, arreglo2, 0, size);
			arreglo = arreglo2;
		}
		arreglo[size] = elemento;
		size++;
		siftUp();
	}

	/**
	 * Retorna pero no remueve el elemento máximo/mínimo del heap.
	 * @return T elemento 
	 */
	public T peek()
	{
		// TODO Auto-generated method stub
		return arreglo[0];
	}

	/**
	 * Retorna el elemento máximo/mínimo luego de removerlo del heap.
	 * @return T El elemento máximo/mínimo del heap
	 */
	public T poll()
	{
		// TODO Auto-generated method stub
		T temp = arreglo[0];
		arreglo[0] = arreglo[size - 1];
		arreglo[size - 1] = null;
		size--;
		siftDown();
		return temp;
	}

	/**
	 * Retorna el número de elementos en el heap
	 * @return size Número de elementos en el heap
	 */
	public int size()
	{

		return size;
	}

	/**
	 * Retorna true si el heap no tiene elementos; false de lo contrario.
	 * @return
	 */
	public boolean isEmpty()
	{
		// TODO Auto-generated method stub

		return arreglo[0] == null;
	}


	/**
	 * Mueve el último elemento arriba en el arbol, mientras que sea necesario.
	 * Es usado para restaurar la condición de heap luego de inserción.
	 */
	public void siftUp()
	{
		// TODO Auto-generated method stub

		int hijo = size - 1;
		int padre = hijo % 2 == 0 ? (hijo / 2) - 1 : ((hijo - 1) / 2);
		if (padre >= 0 && hijo < arreglo.length && arreglo[padre] != null
				&& arreglo[hijo] != null)
		{
			if (orden == null)
			{
				while (hijo != 0 && arreglo[hijo].compareTo(arreglo[padre]) > 0)
				{

					T temp = arreglo[padre];
					arreglo[padre] = arreglo[hijo];
					arreglo[hijo] = temp;

					hijo = padre;

					padre = hijo % 2 == 0 ? (hijo / 2) - 1 : (hijo - 1) / 2;

				}
			} else
			{
				while (hijo != 0
						&& orden.compare(arreglo[hijo], arreglo[padre]) > 0)

				{

					T temp = arreglo[padre];
					arreglo[padre] = arreglo[hijo];
					arreglo[hijo] = temp;

					hijo = padre;

					padre = hijo % 2 == 0 ? (hijo / 2) - 1 : (hijo - 1) / 2;

				}
			}
		}
	}

//	@Override
//	public String toString()
//	{
//		// TODO Auto-generated method stub
//		return Arrays.toString(arreglo);
//	}

	/**
	 * Mueve la raíz abajo en el arbol, mientras que sea necesario.
	 * Es usado para restaurar la condición de heap luego de la eliminación o reemplazo.
	 */
	public void siftDown()
	{
		int head = 0;
		int hijoizq = (head * 2) + 1;
		int hijoder = (head + 1) * 2;
		int mayor = -1;

		if (arreglo[hijoizq] != null && arreglo[hijoder] != null)
		{
			if (orden == null)
			{
				mayor = arreglo[hijoizq].compareTo(arreglo[hijoder]) > 0 ? hijoizq
						: hijoder;

			} else
			{
				mayor = orden.compare(arreglo[hijoizq], arreglo[hijoder]) > 0 ? hijoizq
						: hijoder;
			}
		} else if ((arreglo[hijoder] == null))
		{
			mayor = hijoizq;
		}
		while (mayor != -1
				&& arreglo[head] != null
				&& arreglo[mayor] != null
				&& (orden != null ? orden
						.compare(arreglo[head], arreglo[mayor]) < 0
						: arreglo[head].compareTo(arreglo[mayor]) < 0))
		{

			T temp = arreglo[head];
			arreglo[head] = arreglo[mayor];
			arreglo[mayor] = temp;

			head = mayor;
			hijoizq = (head * 2) + 1;
			hijoder = (head + 1) * 2;

			if (arreglo[hijoizq] != null && arreglo[hijoder] != null)
			{
				if (orden == null)
				{
					mayor = arreglo[hijoizq].compareTo(arreglo[hijoder]) > 0 ? hijoizq
							: hijoder;

				} else
				{
					mayor = orden.compare(arreglo[hijoizq], arreglo[hijoder]) > 0 ? hijoizq
							: hijoder;
				}
			} else if ((arreglo[hijoder] == null))
			{
				mayor = hijoizq;
			} else
			{
				mayor = -1;
			}
		}

	}

//	public static void main(String[] args)
//	{
//		Heap<Integer> heap = new Heap<>(new Comparator<Integer>()
//		{
//			@Override
//			public int compare(Integer i1, Integer i2)
//			{
//				return i2.compareTo(i1);
//			}
//		});
//
//		for (int i = 0; i < 2; i++)
//		{
//			heap.add(i);
//		}
////		System.out.println(heap.poll());
////		System.out.println(heap.poll());
////		 heap.poll();
//
//	}

}